# ABOUT

This repository contains the code for the R Shiny companion application of 
my Medium post: Dynamic UI in Shiny (incl. demo app).

Please comment on the post in case of any feedback/ question.

Many thanks!

Thomas